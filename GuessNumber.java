import javax.swing.*;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in);
		Random rand = new Random();
		int number =rand.nextInt(100);
		int attempts;
		int quit;

		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");
		
		int guess = reader.nextInt();
		attempts = 1;
		quit = 0;
		while (quit != -1) {
			if (guess > number) {
				System.out.println("Sorry!");
				System.out.println("Mine is less than your guess");
				System.out.println("Type -1 to quit or guess another");
				attempts += 1;
			}
			else if (guess < number) {
				System.out.println("Sorry!");
				System.out.println("Mine is greater than your guess: ");
				System.out.println("Type -1 to quit or guess another");
				attempts +=1;
			 }
			else  {
				System.out.print("Congratulations " + "You won after " + attempts + "attempts! ");
			}
			guess = reader.nextInt();
		}


		reader.close();
	}
	
	
}